<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>SMOKE HOOKAH</title>
        <!--CSS de reset das configurações do browser-->
        <link rel="stylesheet" type="text/css" href="css/reset.css">
        <!--CSS de carrossel-->
        <link rel="stylesheet" type="text/css" href="css/slick.css">
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
        <!--CSS de animação-->
        <link rel="stylesheet" type="text/css" href="css/animate.css">	
		<!--LITY-->
		<link rel="stylesheet" type="text/css" href="css/lity.css">
        <!--CSS de estilo da página-->
        <link rel="stylesheet" href="css/estilo.css">
        <link rel="stylesheet" href="rotate.css">
        <link rel="stylesheet" href="css/video.css">
        <style>
                @import url('https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap');
                </style>
		

                <!--Icones favicon-->
                <link rel="apple-touch-icon" sizes="57x57" href="img/icon1/apple-icon-57x57.png">
                <link rel="apple-touch-icon" sizes="60x60" href="img/icon1/apple-icon-60x60.png">
                <link rel="apple-touch-icon" sizes="72x72" href="img/icon1/apple-icon-72x72.png">
                <link rel="apple-touch-icon" sizes="76x76" href="img/icon1/apple-icon-76x76.png">
                <link rel="apple-touch-icon" sizes="114x114" href="img/icon1/apple-icon-114x114.png">
                <link rel="apple-touch-icon" sizes="120x120" href="img/icon1/apple-icon-120x120.png">
                <link rel="apple-touch-icon" sizes="144x144" href="img/icon1/apple-icon-144x144.png">
                <link rel="apple-touch-icon" sizes="152x152" href="img/icon1/apple-icon-152x152.png">
                <link rel="apple-touch-icon" sizes="180x180" href="img/icon1/apple-icon-180x180.png">
                <link rel="icon" type="image/png" sizes="192x192"  href="img/icon1/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="img/icon1/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="img/icon1/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="img/icon/favicon-16x16.png">
                <link rel="manifest" href="/manifest.json">
                <meta name="msapplication-TileColor" content="#ffffff">
                <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
                <meta name="theme-color" content="#ffffff">
    </head>
    <body>
		<section class="fullsize-video-bg">
			<div class="inner">
				<div>
					<img src="img/icon/iconehookah.svg" alt="Logo smoke">
					<header id="topo-fixo">
						<div class="site topo">
							<nav>
								<ul><!--Lista de marcadores-->
									<li>
										<a href="#home">
											<span class="icon iconHome"></span>
											<span>HOME</span>
										</a>
									</li><!-- Fim Item da lista-->
									<li>
										<a href="#sobre">
											<span class="icon iconSobre"></span>
											<span>SOBRE</span>
										</a>
									</li>
									<li>
										<a href="#servico">
											<span class="icon iconServico"></span>
											<span>SERVICO</span>
										</a>
									</li>
									<li>
										<a href="contato.html">
											<span class="icon iconContato"></span>	
											<span>CONTATO</span>
										</a>
									</li>
									<li> 
										 <a href="#dicas">
											<span class="icon iconDicas"></span>	
											<span>DICAS</span>
										</a>
									</li>
								</ul>
							</nav>
						</div>
					</header>
				</div>
			</div>
			<div id="video-viewport">
				<video width="1920" height="1080" autoplay muted loop>
					<source src="tabacaria.mp4" type="video/mp4" />
					
				</video>
			</div>
        </section>
        <section class="sobre wow fadeInUpBig" id="home"><!-- SOBRE -->
            <div class="site">
                <article>
                    <h2>HOOKAH LOUNGE</h2>
                    <p>LOUNGE
                        SAIBA MAIS SOBRE O

                        SMOKE HOOKAH LOUNGE
                        
                        Está cansado dos sabores de essência que tem em casa? Tem um grupo de amigos grande e quer que todos compartilhem uma sessão maravilhosa juntos?
                        
                        O Jardins Hookah Lounge é um novo conceito de tabacaria para quem quer curtir uma sessão de narguile enquanto conversa com os amigos, saboreia uma boa comida e toma uma gelada!
                         
                        
                        Venha nos conhecer!</p>
                </article>
                
                </article>
            </div>
        </section><!-- FIM SOBRE -->
        <section class="depoimento wow fadeInUpBig" id="sobre"><!--depoimento-->
            <div class="site">
                <article>
                    <h1>SOBRE NÓS</h1>
                    <p>                    Smoke Hookah Lounge – uma experiência única para os apreciadores de narguilé.
                        Localizadas na Zona Sul de São Paulo, as três unidades da BS Hookah contam com lounge especializado em narguilé, com estrutura completa que inclui:
                        
                         Ambiente exclusivo, moderno e aconchegante.
                        Um espaço agradável para estar com os amigos dividindo uma sessão de narguilé!
                        Cardápio variado.
                        Cítrico, doce ou mentolado?
                        Nosso cardápio conta com mais de 100 opções de sabores para todos os gostos!
                        Ficou na dúvida?
                        
                        
                        </p>
                </article>
    
            </div>
        </section>
     
               
        <section class="servico wow fadeInUpBig" id="servico"><!--DESTAQUE-->
           

            <div class="site">
           
                <article class="servicoUm">
                    
                    <article><!--destaque 1-->
                            <img src="img/imagens_site/destaque/carv.png" alt="Destaque 1">
                            <h3>Carvões</h3>
                            
                       

                    </article><!--fim destaque 1-->
                    <article><!--destaque 2-->
                            <img src="img/imagens_site/destaque/ess.png" alt="destaque 2">
                            <h3>Essências</h3>
                            
                        
                    </article><!--fim destaque 2-->
                    <article>
                        <img src="img/imagens_site/destaque/drinks.png" alt="destaque 3">
                                <h3>Bebidas</h3>

                    </article>
                    <article><!--destaque 1-->
                        <img src="img/imagens_site/destaque/porcao.png" alt="Destaque 1">
                        <h3>Porções</h3>
                        
                   

                </article><!--fim destaque 1--> 
                
                <article><!--destaque 1-->
                    <img src="img/imagens_site/destaque/narg.png" alt="Destaque 1">
                    <h3>Alugue seu Narguile</h3>
                    
               

            </article><!--fim destaque 1--> 
            
            <article><!--destaque 1-->
                <img src="img/imagens_site/destaque/louge.png" alt="Destaque 1">
                <h3>Espaço Lounge</h3>
                
           

        </article><!--fim destaque 1-->

                </article>
            </div>	
        </section><!--fim destque-->
        <section class="servico wow fadeInUpBig" id="servico"><!--DESTAQUE-->

            <section class="depoimento3 wow fadeInUpBig"><!--depoimento-->
                <div class="site">
                    <article>
                        <h1>Novidades do mês</h1>
                        <p>                    Hookah Lounge – uma experiência única para os apreciadores de narguilé.
                            Localizadas na Zona Sul de São Paulo, as três unidades da BS Hookah contam com lounge especializado em narguilé, com estrutura completa que inclui:
                            
                             Ambiente exclusivo, moderno e aconchegante.
                            Um espaço agradável para estar com os amigos dividindo uma sessão de narguilé!
                            Cardápio variado.
                            Cítrico, doce ou mentolado?
                            Nosso cardápio conta com mais de 100 opções de sabores para todos os gostos!
                            Ficou na dúvida?
                            Nossos atendentes podem sugerir uma essência ou mix, de acordo com as suas preferências.
                            Acessórios e matéria prima top de linha.
                            Trabalhamos com o que há de melhor no mercado hoje – setups e acessórios de qualidade, essências e fluidos das melhores marcas.
                            Excelência no atendimento é nossa prioridade!</p>
                    </article>
        
                </div>
            </section>
           

            <div class="site">
                
                <article class="servicoDois">
                    
                   
                    <article><!--destaque 1-->
                        <img src="img/imagens_site/destaque/role1.png" alt="Destaque 1">
                        
                        
                   

                </article><!--fim destaque 1-->
                <article><!--destaque 1-->
                    <img src="img/imagens_site/destaque/role2.png" alt="Destaque 1">
                 
                    
               

            </article><!--fim destaque 1-->
                    <article><!--destaque 2-->
                            <img src="img/imagens_site/destaque/role3.png" alt="destaque 2">
                            
                            
                        
                    </article><!--fim destaque 2-->
                    <article>
                        <img src="img/imagens_site/destaque/role4.png" alt="destaque 2">
                              
                                			
                    </article>

                    <article>
                        <img src="img/imagens_site/destaque/role5.png" alt="destaque 2">
                                
                                			
                    </article>

                    <article>
                        <img src="img/imagens_site/destaque/role6.png" alt="destaque 2">
                                
                                			
                    </article>

                    <article>
                        <img src="img/imagens_site/destaque/role8.png" alt="destaque 2">
                                
                                			
                    </article>

                    <article>
                        <img src="img/imagens_site/destaque/role9.png" alt="destaque 2">
                                
                    </article>

                    <article>
                        <img src="img/imagens_site/destaque/role10.png" alt="destaque 2">
                                
                                			
                    </article>

                    <article>
                        <img src="img/imagens_site/destaque/role11.png" alt="destaque 2">
                                
                                			
                    </article>

                    <article>
                        <img src="img/imagens_site/destaque/role12.png" alt="destaque 2">
                                
                                			
                    </article>

                </article>
            </div>	
        </section><!--fim destque-->
        
        <section>
            <div class="insta">
                <a href="https://www.instagram.com/hookahlounge4/" >
              <img src="../tabacariasite/img/icon/rodapé/insta.svg" alt="instagram">
            </a>
            </div>
        </section>
       
        

        </section><!--fim destque-->
        
    <section class="depoimentoo wow fadeInUpBig" id="sobre"><!--depoimento-->
        <div class="site">
            <article>
                <h1>Dicas</h1>
                <p>                    Hookah Lounge – uma experiência única para os apreciadores de narguilé.
                    Localizadas na Zona Sul de São Paulo, as três unidades da BS Hookah contam com lounge especializado em narguilé, com estrutura completa que inclui:
                    
                     Ambiente exclusivo, moderno e aconchegante.
                    Um espaço agradável para estar com os amigos dividindo uma sessão de narguilé!
                    Cardápio variado.
                    Cítrico, doce ou mentolado?
                    Nosso cardápio conta com mais de 100 opções de sabores para todos os gostos!
                    Ficou na dúvida?
                    Nossos atendentes podem sugerir uma essência ou mix, de acordo com as suas preferências.
                    Acessórios e matéria prima top de linha.
                    Trabalhamos com o que há de melhor no mercado hoje – setups e acessórios de qualidade, essências e fluidos das melhores marcas.
                    Excelência no atendimento é nossa prioridade!</p>
            </article>

        </div>
    </section>

    <!--fim depoimento-->

    <section class="banner3"><!--banner-->
        <article>
        <a href="https://www.youtube.com/watch?v=88bR5a5ioOo" data-lity>
        <img src="img/util/imgvideo2.jpg" alt="SMOKE HOOKAH" alt="banner SMOKE">
        <h3>VIDEO 1</h3>
        </a>
        </article>
        <article>
            <a href="https://www.youtube.com/watch?v=88bR5a5ioOo" data-lity>
            <img src="img/util/imag1.png" alt="SMOKE HOOKAH" alt="banner SMOKE">
            <h3>VIDEO 2</h3>
            </a>
            </article>
            <article>
                <a href="https://www.youtube.com/watch?v=88bR5a5ioOo" data-lity>
                <img src="img/util/imgvideo3.png" alt="SMOKE HOOKAH" alt="banner SMOKE">
                <h3>VIDEO 3</h3>
                </a>
                </article>
                <article>
                    <a href="https://www.youtube.com/watch?v=88bR5a5ioOo" data-lity>
                    <img src="img/util/imgvideo4.png" alt="SMOKE HOOKAH" alt="banner SMOKE">
                    <h3>VIDEO 4</h3>
                    </a>
                    </article>
                    <article>
                        <a href="https://www.youtube.com/watch?v=88bR5a5ioOo" data-lity>
                        <img src="img/util/imgvideo6.png" alt="SMOKE HOOKAH" alt="banner SMOKE">
                        <h3>VIDEO 5</h3>
                        </a>
                        </article>
                        <article>
                            <a href="https://www.youtube.com/watch?v=88bR5a5ioOo" data-lity>
                            <img src="img/util/imgvideo7.png" alt="SMOKE HOOKAH" alt="banner SMOKE">
                            <h3>VIDEO 6</h3>
                            </a>
                            </article>
                            <article>
                                <a href="https://www.youtube.com/watch?v=88bR5a5ioOo" data-lity>
                                <img src="img/util/imgvideo8.png" alt="SMOKE HOOKAH" alt="banner SMOKE">
                                <h3>VIDEO 7</h3>
                                </a>
                                </article>
    
    </section><!--fim banner-->	
</article>


    </section><!--fim destque-->
   
    

            <div class="site">
                
            </div>

    </section>
  
    <section class="cont">
        <div>
                <a href="https://www.instagram.com/hookahlounge4/">
                    <img src="../tabacariasite/img/icon/rodapé/insta.svg" alt="">
                    </a>
             <a href="https://www.facebook.com/smokeh">
            <img src="../tabacariasite/img/icon/rodapé/face.svg"   alt="">
            </a>
            <a href="https://api.whatsapp.com/send?1=pt_BR&phone=5511964239562">
                <img src="../tabacariasite/img/icon/whatsapp.svg"   alt="">
                </a>
           
        </div>
    </section>
	<footer class="rodape wow fadeInUpBig">
		<div class="site">

		</div>
	</footer>
	<footer class="rodape wow fadeInUpBig"><!--rodapé-->
		<div class="site">
			</div>
			
				<p class="rodape" >Todos os direitos reservados a Smoke Hookah 2019</p>

            </div>
            
	</footer>
                    
    

    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/lity.js"></script>
    <script type="text/javascript" src="js/animacao.js"></script>
    <script type="text/javascript" src="js/video.js"></script>
    </body>
</html>